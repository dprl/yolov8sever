import os
import tempfile
from io import StringIO
from typing import List

from pdf_annotate import PdfAnnotator, Location, Appearance
from starlette.responses import FileResponse
from ultralytics import YOLO
import torch
import PIL as pil
from PIL.Image import Image
from fastapi import File, FastAPI, UploadFile
from pdf2image import convert_from_bytes
import json
from itertools import islice


app = FastAPI()

if torch.cuda.is_available():
    device = torch.device("cuda")
else:
    device = torch.device("cpu")

# load model
model = YOLO(os.path.join("weights", "best.pt"))



@app.post("/yolo")
async def extract(dpi: int, conf: float = 0.25, iou_thresh: float = 0.5,
                  pdf: bytes = File(..., media_type="application/pdf")):
    images: List[Image] = convert_from_bytes(pdf_file=pdf, dpi=dpi)

    size_dict = {}
    for i, image in enumerate(images):
        size_dict[i] = (image.height, image.width)

    results = model.predict(images, conf=conf, iou=iou_thresh, device=device)
    csv_buffer = StringIO()
    for i, img_result in enumerate(results):
        boxes = img_result.boxes
        for box in boxes:
            b = box.xyxy[0]
            csv_buffer.write(f"{i},{b[0]},{b[1]},{b[2]},{b[3]}\n")

    return {"size_dict": size_dict, "csv": csv_buffer.getvalue()}


@app.post("/yolo_from_images")
async def extract(dpi: int = 300, conf: float = 0.25, iou_thresh: float = 0.5,
                  images: List[UploadFile] = File(...), chunk_size: int = 40):
    
    print(images, "images")
    images_pil: List[Image] = [pil.Image.open(i.file) for i in images]
    csv_buffer = StringIO()
    
    # Helper function to get chunks of a given size
    def chunked(iterable, size):
        iterator = iter(iterable)
        while chunk := list(islice(iterator, size)):
            yield chunk
    
    # Process images in chunks to avoid overloading the GPU
    for chunk_index, chunk in enumerate(chunked(images_pil, chunk_size)):
        results = model.predict(chunk, conf=conf, iou=iou_thresh, device=device)
        for j, img_result in enumerate(results):
            image_index = chunk_index * chunk_size + j
            boxes = img_result.boxes
            for box in boxes:
                b = box.xyxy[0]
                csv_buffer.write(f"{image_index},{b[0]},{b[1]},{b[2]},{b[3]}\n")
        print("Processed chunk", chunk_index)
    
    print("csv_buffer.getvalue()", csv_buffer.getvalue())
    return {"csv": csv_buffer.getvalue()}

@app.post("/yolo_from_images")
async def extract(dpi: int=300, conf: float = 0.25, iou_thresh: float = 0.5,
                  images: List[UploadFile] = File(...)):
    
    print(images,"images")
    images_pil: List[Image] = []
    images_pil: List[Image] = [pil.Image.open(i.file) for i in images]

    results = model.predict(images_pil, conf=conf, iou=iou_thresh, device=device)
    csv_buffer = StringIO()
    for i, img_result in enumerate(results):
        boxes = img_result.boxes
        for box in boxes:
            b = box.xyxy[0]
            csv_buffer.write(f"{i},{b[0]},{b[1]},{b[2]},{b[3]}\n")
    print("csv_buffer.getvalue()",csv_buffer.getvalue())
    return {"csv":csv_buffer.getvalue()}

@app.post("/visualize")
async def visualize(dpi: int, conf: float = 0.3, iou_thresh: float = 0.4, pdf: bytes = File(..., media_type="application/pdf")):
    res = await extract(dpi, conf, iou_thresh, pdf)
    pdf_file_out_path = tempfile.NamedTemporaryFile(delete=False).name
    with tempfile.TemporaryDirectory() as td:
        pdf_file_path = os.path.join(td, "test.pdf")
        with open(pdf_file_path, "wb") as f:
            f.write(pdf)
        a = PdfAnnotator(pdf_file_path)

        for line in res["csv"].split('\n'):
            parts = line.split(",")
            if len(parts) < 2:
                continue
            page = int(float(parts[0]))
            bounds = a.get_page_bounding_box(page)

            origin_x = bounds[0]
            origin_y = bounds[1]

            page_size = res["size_dict"][page]
            height = float(page_size[0])

            # need to perform inversion due to PDF origin being bottom left as opposed to image origin of top right
            top_l_x = float(parts[1]) * 72 / 300 - origin_x
            top_l_y = (height - float(parts[4])) * 72 / 300 - origin_y
            bottom_r_x = float(parts[3]) * 72 / 300 - origin_x
            bottom_r_y = (height - float(parts[2])) * 72 / 300 - origin_y

            a.add_annotation(
                'square',
                Location(x1=top_l_x, y1=top_l_y, x2=bottom_r_x, y2=bottom_r_y, page=page),
                Appearance(stroke_color=(1, 0, 0), stroke_width=1),
            )
        a.write(pdf_file_out_path)
        return FileResponse(
            pdf_file_out_path,
            media_type='application/pdf',
            filename="visualized"
        )

@app.get("/get_version")
def get_version():    
    root = os.getcwd()
    with open(os.path.join(root,".cz.json")) as f:
        config = json.load(f)
    return config["commitizen"]["version"]
