FROM ultralytics/ultralytics:latest

COPY . .

RUN apt-get update
RUN apt-get install poppler-utils -y

RUN pip install -r requirements.txt

EXPOSE 8005

CMD uvicorn server:app --port 8005 --host 0.0.0.0
