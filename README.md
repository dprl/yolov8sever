# DPRL Chemical Diagram Detector Based on [yoloV8](https://github.com/ultralytics/ultralytics/tree/main)

Server with endpoints to detect and visualize chemical diagrams within PDFs.

## Installation
### Local 
Run the following commands to create a conda environment with the necessary dependencies.
```commandline
conda create -n yolov8 python=3.10
conda activate yolov8
pip install -r requiremnts.txt
```
### Docker 
A docker image with the yolo server packaged with all its dependencies exists already within the [DPRL Dockerhub](https://hub.docker.com/u/dprl) 

Assuming docker is already installed, simply run
```commandline
docker pull dprl/yolov8server:latest
```
## Server
### Local
To start the server locally run
```commandline
conda activate yolov8
uvicorn server:app --port 8005
```
Go to `http://127.0.0.1:8005/docs` to see the Swagger API documentation
### Docker
To start the server from docker run
```commandline
docker run -p 8005:8005 dprl/yolov8server
```
Go to `http://127.0.0.1:8005/docs` to see the Swagger API documentation
## Server Building

To build a new docker image which reflects any changes made run the following command:
```commandline
docker buildx build . -t dprl/yolov8server:latest
```

## Training

Run the following commands which will download the necessary data and start the training process

```commandline
./training_data.sh
conda activate yolov8
python train.py
```

